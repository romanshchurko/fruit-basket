<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "basket".
 *
 * @property integer $id
 * @property string $name
 * @property string $capacity
 *
 * @property BasketItems[] $basketItems
 */
class Basket extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'basket';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'capacity'], 'required'],
            [['capacity', 'capacity_remaind'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'capacity' => 'Capacity',
            'capacity_remaind' => 'Capacity remaind',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBasketItems() {
        return $this->hasMany(BasketItems::className(), ['basket_id' => 'id']);
    }

    public function fields() {
        return [
            'id' => 'id',
            'name' => 'name',
            'capacity' => 'capacity',
            'capacity_remaind' => 'capacity_remaind',
            'contents' => function () {
                return $this->basketItems;
            },
        ];
    }

    public function beforeDelete() {
        \app\models\BasketItems::deleteAll(['basket_id' => $this->id]);
        return parent::beforeDelete();
    }

}
