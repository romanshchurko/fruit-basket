<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "basket_items".
 *
 * @property integer $id
 * @property integer $basket_id
 * @property integer $type
 * @property string $weight
 *
 * @property TypeItem $type0
 * @property Basket $basket
 */
class BasketItems extends \yii\db\ActiveRecord {
    
    public $maxWeight;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'basket_items';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['basket_id', 'type', 'weight'], 'required'],
            [['basket_id', 'type'], 'integer'],
            [['weight'], 'number'],
//            [['type'], 'exist', 'skipOnError' => true, 'targetClass' => TypeItem::className(), 'targetAttribute' => ['type' => 'id']],
//            [['basket_id'], 'exist', 'skipOnError' => true, 'targetClass' => Basket::className(), 'targetAttribute' => ['basket_id' => 'id']],
            [['weight'], 'weightValidator'],
            [['maxWeight'], 'weightMaxValidator', 'on' => 'maxWeight'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'basket_id' => 'Basket ID',
            'type' => 'Type',
            'weight' => 'Weight',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0() {
        return $this->hasOne(TypeItem::className(), ['id' => 'type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBasket() {
        return $this->hasOne(Basket::className(), ['id' => 'basket_id']);
    }

    public function weightValidator($k, $v) {
        $basket = Basket::findOne($this->basket_id);

        if (isset($basket->capacity_remaind) && $basket->capacity_remaind == 0) {
            $this->addError($k, "Basket is full");
        } else if (isset($basket->capacity_remaind) && $basket->capacity_remaind < $this->$k) {
            $this->addError($k, "Weight must be no greater than $basket->capacity_remaind.");
        }
    }
   
    public function weightMaxValidator($k, $v) {
        $basket = Basket::findOne($this->basket_id);

        if (isset($basket->capacity_remaind) && $basket->capacity_remaind == 0) {
            $this->addError($k, "Basket is full");
        } else if (isset($basket->capacity_remaind) && $basket->capacity_remaind < $this->$k) {
            $this->addError($k, "MAX Weight must be no greater than $basket->capacity_remaind.");
        }
    }    
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $this->BasketCapacityRemaind($this->basket_id, $this->weight, TRUE);
    }

    public function afterDelete() {
        parent::afterDelete();
        $this->BasketCapacityRemaind($this->basket_id, $this->weight, FALSE);
    }

    private function BasketCapacityRemaind($id, $weight, $type) {
        $basket = Basket::findOne($id);

        if ($basket) {
            if ($type)
                $weight = -1 * $weight;

            $basket->capacity_remaind = $basket->capacity_remaind + $weight;
            $basket->save();
        }
    }

}
