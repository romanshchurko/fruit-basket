<?php

namespace app\controllers;

use Yii;
use app\models\Basket;
use app\models\BasketItems;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * BasketController implements the CRUD actions for Basket model.
 */
class BasketController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'deleteItem' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Render home page.
     */    
    public function actionHome() {
        return $this->render('home');
    }

    /**
     * Render raport page.
     */    
    public function actionRaport() {
        return $this->render('raport');
    }    
    
    /**
     * Lists all Basket models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Basket::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Basket model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Basket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Basket();

        if ($model->load(Yii::$app->request->post())) {
            $model->capacity_remaind = $model->capacity;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new BasketItems model.
     * If creation is successful, the browser will be redirected to the 'view' basket page.
     * @return mixed
     */
    public function actionAddItem($id) {
        $model = new BasketItems();

        if ($model->load(Yii::$app->request->post())) {
            $model->basket_id = $id;

            if ($model->save()) {
                return $this->render('view', [
                            'model' => $this->findModel($id),
                ]);
            }
        }

        return $this->render('_form_add_item', [
                    'id' => $id,
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BasketItems model.
     * If deletion is successful, the browser will be redirected to the 'view' basket page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteItem($id) {
        $model = BasketItems::findOne($id);
        $basketId = $model->basket_id;

        if ($model->delete()) {
            return $this->redirect(['view', 'id' => $basketId]);
        }

        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Basket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save(true, ['name'])) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Basket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Basket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Basket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Basket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSelectBasketItems() {
        $items = BasketItems::findAll(['basket_id' => Yii::$app->request->post('basket_id')]);

        if ($items) {
            foreach ($items as $i) {
                echo "<option value='" . $i->id . "'>" . $i->type0->name . " - " . $i->weight . "</option>";
            }
        } else {
            echo "<option value=''>no items</option>";
        }
    }

    public function actionBasketName() {
        $basket = Basket::findOne(Yii::$app->request->post('basket_id'));

        if ($basket) {
            return json_encode(['name' => $basket->name]);
        }

        return json_encode(['error' => 'Failed to delete the object for unknown reason.']);
    }

}
