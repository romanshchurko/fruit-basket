<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class ManualController extends Controller {
    
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionAll() {
        return $this->render('_all');
    }

    public function actionView() {
        return $this->render('_view');
    }

    public function actionDelete() {
        return $this->render('_delete');
    }

    public function actionDeleteItem() {
        return $this->render('_delete_item');
    }

    public function actionRename() {
        return $this->render('_rename');
    }   
    
    public function actionAddBasket() {
        return $this->render('_add_basket');
    }      
    
    public function actionAddItems() {
        return $this->render('_add_items');
    }    
}
