<?php

namespace app\modules\api;

use Yii;
use yii\web\Response;

class Module extends \yii\base\Module {

    public $controllerNamespace = 'app\modules\api\controllers';

}
