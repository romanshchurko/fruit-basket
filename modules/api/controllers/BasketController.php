<?php

namespace app\modules\api\controllers;

use app\models\Basket;
use app\models\BasketItems;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class BasketController extends Controller {

    public function behaviors() {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex() {
        return new ActiveDataProvider([
            'query' => Basket::find(),
        ]);
    }

    public function actionView($id) {
        $model = Basket::findOne($id);
        return $model;
    }

    public function actionAddBasket() {
        $model = new Basket();

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->capacity_remaind = $model->capacity;

        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }

    public function actionRenameBasket($basket_id) {
        $model = Basket::findOne($basket_id);

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->save(true, ['name'])) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }

    public function actionDelete($id) {
        $basket = Basket::find()->where(['id' => $id])->one();

        if ($basket->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        $response = Yii::$app->getResponse();
        $response->setStatusCode(204);
    }

    public function actionAddItem($basket_id) {
        $request = Yii::$app->getRequest()->getBodyParams();
        $weight = 0;
        $models = $modelsResponse = [];

        if (isset($request['BasketItems']) && !empty($request['BasketItems'])) {

            foreach ($request['BasketItems'] as $k => $item) {
                $model = new BasketItems(['scenario' => 'maxWeight']);
                $model->basket_id = $item['basket_id'] = $basket_id;
                $model->type = $item['type'];
                $model->weight = $item['weight'];
                $weight += $item['weight'];
                $model->maxWeight = $weight;

                if (!$model->validate()) {
                    Yii::$app->getResponse()->setStatusCode(422);
                    return $model;
                }

                $models[] = $model;
            }

            if ($models) {
                foreach ($models as $model) {
                    if ($model->save(FALSE))
                        $modelsResponse[] = $model;
                }
            }
        } else {
            throw new ServerErrorHttpException('No input data.');
        }

        if ($modelsResponse) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
        }

        return $modelsResponse;
    }

    public function actionRemoveItem($item_id) {
        $item = BasketItems::find()->where(['id' => $item_id])->one();

        if ($item->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        $response = Yii::$app->getResponse();
        $response->setStatusCode(204);
    }

}
