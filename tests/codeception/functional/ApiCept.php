<?php

/* @var $scenario Codeception\Scenario */

$I = new FunctionalTester($scenario);
$I->wantTo('ensure that rest api works');

// Add new basket.
$I->sendPOST('/basket/add-basket', ['name' => 'test basket via api', 'capacity' => 10]);
$I->seeResponseCodeIs(201);
$I->seeResponseIsJson();

// View list of existent baskets
$I->sendGET('/basket');
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();

// View basket and contents
$basket = $I->grabRecord('app\models\Basket');
$I->sendGET("/basket/{$basket->id}");
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();

// Rename basket.
$basket = $I->grabRecord('app\models\Basket', ['name' => 'test basket via api']);
$I->sendPOST("/basket/rename-basket?basket_id={$basket->id}", ['name' => 'rename basket via api']);
$I->seeResponseCodeIs(201);
$I->seeResponseIsJson();

$I->sendPOST("/basket/add-item?basket_id={$basket->id}", [ 'BasketItems' => [
        ['type' => '1', 'weight' => 2],
        ['type' => '2', 'weight' => 5],
        ['type' => '3', 'weight' => 3]
    ]
]);
$I->seeResponseCodeIs(201);
$I->seeResponseIsJson();

// check max weight validation
$I->sendPOST("/basket/add-item?basket_id={$basket->id}", [ 'BasketItems' => [
        ['type' => '1', 'weight' => 2],
        ['type' => '2', 'weight' => 5],
        ['type' => '3', 'weight' => 3]
    ]
]);
$I->seeResponseCodeIs(422);
$I->seeResponseIsJson();
$I->canSeeResponseContainsJson(['field' => 'weight', "message" => "Basket is full"]);

// Remove item from basket.
$item = $I->grabRecord('app\models\BasketItems', ['basket_id' => $basket->id]);
$I->sendPOST("/basket/remove-item/?item_id={$item->id}");
$I->seeResponseCodeIs(204);

// Remove basket with all contents.
$item = $I->grabRecord('app\models\BasketItems', ['basket_id' => $basket->id]);
$I->sendGET("/basket/delete/{$basket->id}");
$I->seeResponseCodeIs(204);