<?php

namespace tests\codeception\unit\models;

use Yii;
use yii\codeception\TestCase;
use Codeception\Specify;
use app\models\Basket;

class BasketTest extends TestCase
{
    use Specify;

    protected function setUp()
    {
        parent::setUp();
        Basket::deleteAll();
        $this->basket = new Basket();        
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    public function testValidation() {

        $this->specify('fields are required', function() {
            $this->basket->name = "";
            $this->basket->capacity = "";
            expect("model is not valid", $this->basket->validate())->false();
            expect("name has error", $this->basket->getErrors())->hasKey('name');
            expect("capacity has error", $this->basket->getErrors())->hasKey('capacity');
        });

        $this->specify('fields are correct', function() {
            $this->basket->name = "test basket";
            $this->basket->capacity = 10;
            expect("model is valid", $this->basket->validate())->true();
        });
    }

    public function testSaveIntoDataBase() {
        $this->basket = new Basket([
            'name' => 'new test basket',
            'capacity' => 3,
        ]);

        expect("model is saved", $this->basket->save())->true();
    }

}
