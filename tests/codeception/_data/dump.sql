CREATE TABLE IF NOT EXISTS `basket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capacity` decimal(15,2) NOT NULL,
  `capacity_remaind` decimal(15,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

INSERT INTO `basket` (`id`, `name`, `capacity`, `capacity_remaind`) VALUES
(1, 'тетова корзина', '10.00', '1.00');

CREATE TABLE IF NOT EXISTS `basket_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `basket_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` decimal(15,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `basket_items_id` (`basket_id`),
  KEY `type_item` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

INSERT INTO `basket_items` (`id`, `basket_id`, `type`, `weight`) VALUES
(10, 1, 1, '5.00'),
(12, 1, 2, '3.00'),
(13, 1, 1, '1.00');

