<?php

use tests\codeception\_pages\ApiManualPage;

/* @var $scenario Codeception\Scenario */

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that api manual works');
ApiManualPage::openBy($I);
$I->see('Api', 'th');
