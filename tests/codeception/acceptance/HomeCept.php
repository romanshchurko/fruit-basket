<?php

/* @var $scenario Codeception\Scenario */

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that home page works');
$I->amOnPage(Yii::$app->homeUrl);
$I->see('TEST TASK: FRUIT BASKET API');
$I->seeLink('Api');
$I->click('Api');
$I->see('Api','th');
