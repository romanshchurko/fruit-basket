<?php

use yii\db\Schema;
use yii\db\Migration;

class m160526_122958_basket extends Migration {

    public function up() {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%basket}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' not null',
            'capacity' => Schema::TYPE_DECIMAL . '(15,2) not null',
            'capacity_remaind' => Schema::TYPE_DECIMAL . '(15,2) default 0',
                ], $tableOptions);

        $this->createTable('{{%basket_items}}', [
            'id' => Schema::TYPE_PK,
            'basket_id' => Schema::TYPE_INTEGER . ' not null',
            'type' => Schema::TYPE_INTEGER . ' not null',
            'weight' => Schema::TYPE_DECIMAL . '(15,2) not null',
                ], $tableOptions);

        $this->createTable('{{%type_item}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' not null',
                ], $tableOptions);

        // insert type item
        $columns = ['name'];
        $this->batchInsert('{{%type_item}}', $columns, [
            ['apple'],
            ['orange'],
            ['watermelon'],
        ]);

//        $this->addForeignKey('{{%basket_items_id}}', '{{%basket_items}}', 'basket_id', '{{%basket}}', 'id');
//        $this->addForeignKey('{{%type_item}}', '{{%basket_items}}', 'type', '{{%type_item}}', 'id');
    }

    public function down() {
//        $this->dropForeignKey('{{%basket_items_id}}', '{{%basket_items}}');
//        $this->dropForeignKey('{{%type_item}}', '{{%basket_items}}');
        $this->dropTable('{{%basket}}');
        $this->dropTable('{{%basket_items}}');
        $this->dropTable('{{%type_item}}');
    }

}
