$(document).on('click', '.runIndex', function() {
    var url = $(this).data('url');

    $.ajax({
        url: url,
        method: 'GET'
    }).then(function(data) {
        var str = JSON.stringify(data, null, '\t')
        $('#result').html(
                str.replace(/\n/g, '<br/>')
                .replace(/\\n/g, ' ')
                .replace(/\t/g, '&nbsp;&nbsp;')
                );
    });
});

$(document).on('click', '.runView', function() {
    var url = $(this).data('url');
    var basket_id = $("select[name='basket']").val();
    url = url.replace("{id}", basket_id);

    if (!basket_id) {
        $("select[name='basket']").css({'border': '2px solid red'});
        $('#result').html('Choose basket');
        return false;
    } else {
        $("select[name='basket']").css({'border': '1px solid #ccc'});
    }

    $.ajax({
        url: url,
        method: 'GET'
    }).then(function(data) {
        var str = JSON.stringify(data, null, '\t')
        $('#result').html(
                str.replace(/\n/g, '<br/>')
                .replace(/\\n/g, ' ')
                .replace(/\t/g, '&nbsp;&nbsp;')
                );
    });
});

$(document).on('click', '.runDelete', function() {
    var url = $(this).data('url');
    var basket_id = $("select[name='basket']").val();
    url = url.replace("{id}", basket_id);

    if (!basket_id) {
        $("select[name='basket']").css({'border': '2px solid red'});
        $('#result').html('Choose basket');
        return false;
    } else {
        $("select[name='basket']").css({'border': '1px solid #ccc'});
    }

    $.ajax({
        url: url,
        method: 'GET',
        statusCode: {
            204: function(xhr) {
                $('#result').html("Status 204.");
            },
            404: function(xhr) {
                var str = JSON.stringify(xhr, null, '\t')
                $('#result').html(
                        str.replace(/\n/g, '<br/>')
                        .replace(/\\n/g, ' ')
                        .replace(/\t/g, '&nbsp;&nbsp;')
                        );
            }
        }
    });
});

$(document).on('click', '.runDeleteItem', function() {
    var url = $(this).data('url');
    var basket_id = $("select[name='basket']").val();
    var item_id = $("select[name='item']").val();
    url = url.replace("{id}", item_id);

    if (!basket_id) {
        $("select[name='basket']").css({'border': '2px solid red'});
        $('#result').html('Choose basket');
        return false;
    } else {
        $("select[name='basket']").css({'border': '1px solid #ccc'});
    }

    if (!item_id) {
        $("select[name='item']").css({'border': '2px solid red'});
        $('#result').html('Choose item');
        return false;
    } else {
        $("select[name='item']").css({'border': '1px solid #ccc'});
    }

    $.ajax({
        url: url,
        method: 'GET',
        statusCode: {
            204: function(xhr) {
                $('#result').html("Status 204.");
            },
            404: function(xhr) {
                var str = JSON.stringify(xhr, null, '\t')
                $('#result').html(
                        str.replace(/\n/g, '<br/>')
                        .replace(/\\n/g, ' ')
                        .replace(/\t/g, '&nbsp;&nbsp;')
                        );
            }
        }
    });
});

function getBasketItems() {
    var basket_id = $("select[name='basket']").val();
    var csrf = $("input[name='_csrf']").val();

    $.ajax({
        type: "POST",
        url: "/basket/select-basket-items",
        data: {basket_id: basket_id, _csrf: csrf},
        success: function(response) {
            if (response) {
                $("select[name=\'item\']").html(response);
            }
        }
    });
}

function getBasketName() {
    var basket_id = $("select[name='basket']").val();
    var csrf = $("input[name='_csrf']").val();

    $.ajax({
        type: "POST",
        url: "/basket/basket-name",
        data: {basket_id: basket_id, _csrf: csrf},
        dataType: 'json',
        success: function(response) {
            if (response.name) {
                $("input[name=\'nameBasket\']").val(response.name);
            }
        }
    });
}

$(document).on('click', '.runRenameBasket', function() {
    var url = $(this).data('url');
    var basket_id = $("select[name='basket']").val();
    url = url.replace("{id}", basket_id);

    if (!basket_id) {
        $("select[name='basket']").css({'border': '2px solid red'});
        $('#result').html('Choose basket');
        return false;
    } else {
        $("select[name='basket']").css({'border': '1px solid #ccc'});
    }

    $.ajax({
        url: url,
        method: 'POST',
        data: {name: $("input[name=\'nameBasket\']").val()},
        statusCode: {
            201: function(xhr) {
                $('#result').html("Status 201.");
            },
            404: function(xhr) {
                var str = JSON.stringify(xhr, null, '\t')
                $('#result').html(
                        str.replace(/\n/g, '<br/>')
                        .replace(/\\n/g, ' ')
                        .replace(/\t/g, '&nbsp;&nbsp;')
                        );
            },
            422: function(xhr) {
                var str = JSON.stringify(xhr, null, '\t')
                $('#result').html(
                        str.replace(/\n/g, '<br/>')
                        .replace(/\\n/g, ' ')
                        .replace(/\t/g, '&nbsp;&nbsp;')
                        );
            }
        }
    }).then(function(data) {
        var str = JSON.stringify(data, null, '\t')
        $('#result').html(
                str.replace(/\n/g, '<br/>')
                .replace(/\\n/g, ' ')
                .replace(/\t/g, '&nbsp;&nbsp;')
                );
    });
});

$(document).on('click', '.runAddBasket', function() {
    var url = $(this).data('url');

    $.ajax({
        url: url,
        method: 'POST',
        data: {name: $("input[name=\'nameBasket\']").val(), capacity: $("input[name=\'capacityBasket\']").val()},
        statusCode: {
//            201: function(xhr) {
//                $('#result').html("Status 201.");
//            },
            404: function(xhr) {
                var str = JSON.stringify(xhr, null, '\t')
                $('#result').html(
                        str.replace(/\n/g, '<br/>')
                        .replace(/\\n/g, ' ')
                        .replace(/\t/g, '&nbsp;&nbsp;')
                        );
            },
            422: function(xhr) {
                var str = JSON.stringify(xhr, null, '\t')
                $('#result').html(
                        str.replace(/\n/g, '<br/>')
                        .replace(/\\n/g, ' ')
                        .replace(/\t/g, '&nbsp;&nbsp;')
                        );
            }
        }
    }).then(function(data) {
        var str = JSON.stringify(data, null, '\t')
        $('#result').html(
                str.replace(/\n/g, '<br/>')
                .replace(/\\n/g, ' ')
                .replace(/\t/g, '&nbsp;&nbsp;')
                );
    });
});

$(document).on('click', '.addItem', function() {
    var item = $('.itemBlock').html();
    $('.basket_items').append(item);
    return false;
});

$(document).on('click', '.removeItem', function() {
    $(this).parents('.item').remove();
    return false;
});

$(document).on('click', '.runAddItems', function() {
    var url = $(this).data('url');
    var basket_id = $("select[name='basket']").val();
    url = url.replace("{id}", basket_id);

    if (!basket_id) {
        $("select[name='basket']").css({'border': '2px solid red'});
        $('#result').html('Choose basket');
        return false;
    } else {
        $("select[name='basket']").css({'border': '1px solid #ccc'});
    }

    $.ajax({
        url: url,
        method: 'POST',
        data: {'BasketItems': createJSON()},
        statusCode: {
//            201: function(xhr) {
//                $('#result').html("Status 201.");
//            },
            404: function(xhr) {
                var str = JSON.stringify(xhr, null, '\t')
                $('#result').html(
                        str.replace(/\n/g, '<br/>')
                        .replace(/\\n/g, ' ')
                        .replace(/\t/g, '&nbsp;&nbsp;')
                        );
            },
            422: function(xhr) {
                var str = JSON.stringify(xhr, null, '\t')
                $('#result').html(
                        str.replace(/\n/g, '<br/>')
                        .replace(/\\n/g, ' ')
                        .replace(/\t/g, '&nbsp;&nbsp;')
                        );
            }
        }
    }).then(function(data) {
        var str = JSON.stringify(data, null, '\t')
        $('#result').html(
                str.replace(/\n/g, '<br/>')
                .replace(/\\n/g, ' ')
                .replace(/\t/g, '&nbsp;&nbsp;')
                );
    });
});

function createJSON() {
    jsonObj = [];
    $(".basket_items .item").each(function() {

        var type = $(this).find('select[name="type"]').val();
        var weight = $(this).find('input[name="weight"]').val();

        item = {}
        item ["type"] = type;
        item ["weight"] = weight;

        jsonObj.push(item);
    });
    return jsonObj;
}