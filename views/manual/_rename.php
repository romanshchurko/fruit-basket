<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Rename basket';
$this->params['breadcrumbs'][] = ['label' => 'Api', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="col-md-4">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
        <div>
            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
            <?=
            Html::dropDownList(
                    'basket', '', ArrayHelper::map(\app\models\Basket::find()->all(), 'id', 'name'), [
                'prompt' => 'Choose basket',
                'class' => 'form-control',
                'onchange' => 'getBasketName()'
            ])
            ?>
        </div>
        <br />        
        <div>
            <?=
            Html::input('', 'nameBasket', '', [
                'class' => 'form-control',
                'placeholder' => 'New name'
            ])
            ?>
        </div>
        <br />
        <div>
            <b>Description:</b> Rename basket.<br />
            <b>URL:</b> /api/basket/rename-basket?basket_id={id} <br />
            <a class="btn btn-success runRenameBasket" data-url="/api/basket/rename-basket?basket_id={id}">Rename</a>
        </div>
    </div>
    <div class="col-md-8">
        <div id="result">Result</div>          
    </div>
</div>