<?php
$this->title = 'List of existent baskets';
$this->params['breadcrumbs'][] = ['label' => 'Api', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-3">
        <div>
            <b>Description:</b> Response all data. <br />
            <b>URL:</b> /api/basket <br />
            <a class="btn btn-success runIndex" data-url="/api/basket">Run</a>
        </div>
         <br />
        <div>
            <b>Description:</b> Response id, name and contents. <br />
            <b>URL:</b> /api/basket?fields=id,name,contents <br />
            <a class="btn btn-success runIndex" data-url="/api/basket?fields=id,name,contents">Run</a>
        </div>
    </div>
    <div class="col-md-9">
        <div id="result">Result</div>          
    </div>
</div>
