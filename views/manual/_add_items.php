<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Add items to basket';
$this->params['breadcrumbs'][] = ['label' => 'Api', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="col-md-4">       
        <div>
            <?=
            Html::dropDownList(
                    'basket', '', ArrayHelper::map(\app\models\Basket::find()->all(), 'id', 'name'), [
                'prompt' => 'Choose basket',
                'class' => 'form-control',
            ])
            ?>
        </div>

        <br />

        <div class="basket_items"></div>

        <div>
            <a href="#" title="Add item" class="addItem"><span class="glyphicon glyphicon-plus"></span> Add item</a>
            <br />            
            <b>Description:</b> Add new items.<br />
            <b>Example post data:</b><br /> 
            <code>
                {
                    "BasketItems": [{
                        "type": "type_id",
                        "weight": "value"
                    }, {
                        "type": "type_id",
                        "weight": "value"
                    }]
                }
            </code>
            .<br />
            <b>URL:</b> /api/basket/add-item?basket_id={id} <br />
            <a class="btn btn-success runAddItems" data-url="/api/basket/add-item?basket_id={id}">Run</a>
        </div>
    </div>
    <div class="col-md-8">
        <div id="result">Result</div>          
    </div>
</div>

<div class="itemBlock hidden">
    <div class="item">
        <div>
            <?=
            Html::dropDownList(
                    'type', '', ArrayHelper::map(\app\models\TypeItem::find()->all(), 'id', 'name'), [
                'prompt' => 'Choose type',
                'class' => 'form-control',
            ])
            ?>
        </div>                
        <div>
            <?=
            Html::input('', 'weight', '', [
                'class' => 'form-control',
                'placeholder' => 'Weight'
            ])
            ?>
        </div> 
        <a href="#" title="Remove item" class="removeItem">Remove item</a>
    </div>
    <br />
</div>