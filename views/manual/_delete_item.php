<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Delete basket';
$this->params['breadcrumbs'][] = ['label' => 'Api', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="col-md-4">
        <div>
            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
            <?=
            Html::dropDownList(
                    'basket', '', ArrayHelper::map(\app\models\Basket::find()->all(), 'id', 'name'), [
                'prompt' => 'Choose basket',
                'class' => 'form-control',
                'onchange' => 'getBasketItems()'
            ])
            ?>
        </div>
        <br />
        <div>
            <?=
            Html::dropDownList(
                    'item', '', [], ['prompt' => 'Choose item', 'class' => 'form-control'])
            ?>
        </div>        
        <br />
        <div>
            <b>Description:</b> Delete item from basket. Response status 204 the response contains no body content.<br />
            <b>URL:</b> /api/basket/remove-item?item_id={id} <br />
            <a class="btn btn-success runDeleteItem" data-url="/api/basket/remove-item?item_id={id}">Run</a>
        </div>
    </div>
    <div class="col-md-8">
        <div id="result">Result</div>          
    </div>
</div>
