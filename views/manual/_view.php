<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'View basket';
$this->params['breadcrumbs'][] = ['label' => 'Api', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="col-md-4">
        <div>
            <?=
            Html::dropDownList(
                    'basket', '', ArrayHelper::map(\app\models\Basket::find()->all(), 'id', 'name'), ['prompt' => 'Choose basket', 'class' => 'form-control'])
            ?>
        </div>
        <br />
        <div>
            <b>Description:</b> Response all data. <br />
            <b>URL:</b> /api/basket/view/{id} <br />
            <a class="btn btn-success runView" data-url="/api/basket/view/{id}">Run</a>
        </div>
        <br />
        <div>
            <b>Description:</b> Response id, name and contents. <br />
            <b>URL:</b> /api/basket/view/{id}?fields=id,name,contents <br />
            <a class="btn btn-success runView" data-url="/api/basket/view/{id}?fields=id,name,contents">Run</a>
        </div>
    </div>
    <div class="col-md-8">
        <div id="result">Result</div>          
    </div>
</div>
