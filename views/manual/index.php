<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = 'Api';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="basket-index">
    <table class="table">
        <thead>
            <tr>
                <th>Api</th><th>Action</th><th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><b>View list of existent baskets</td>
                <td>/api/basket</td>
                <td><a href="/manual/all" class="btn btn-primary">view</a></td>
            </tr>
            <tr>
                <td><b>View basket and contents</td>
                <td>/api/basket/view/{id}</td>
                <td><a href="/manual/view" class="btn btn-primary">view</a></td>
            </tr>
            <tr>
                <td><b>Add new basket.</td>
                <td>/api/basket/add-basket</td>
                <td><a href="/manual/add-basket" class="btn btn-primary">view</a></td>
            </tr>
            <tr>
                <td><b>Rename basket.</td>
                <td>/api/basket/rename-basket?basket_id={id}</td>
                <td><a href="/manual/rename" class="btn btn-primary">view</a></td>
            </tr>
            <tr>
                <td><b>Remove basket with all contents.</td>
                <td>/api/basket/delete/{id}</td>
                <td><a href="/manual/delete" class="btn btn-primary">view</a></td>
            </tr>    
            <tr>
                <td><b>Add the item to the basket.</td>
                <td>/api/basket/add-item?basket_id={id}</td>
                <td><a href="/manual/add-items" class="btn btn-primary">view</a></td>
            </tr>  
            <tr>
                <td><b>Remove item from basket.</td>
                <td>/api/basket/remove-item/?item_id={id}</td>
                <td><a href="/manual/delete-item" class="btn btn-primary">view</a></td>
            </tr>             
        </tbody>
    </table>

    <p>The following list summarizes the HTTP status code that are used by the Yii REST framework:</p>
    <ul>
        <li><code>200</code>: OK. Everything worked as expected.</li>
        <li><code>201</code>: A resource was successfully created in response to a <code>POST</code> request. The <code>Location</code> header
            contains the URL pointing to the newly created resource.</li>
        <li><code>204</code>: The request was handled successfully and the response contains no body content (like a <code>DELETE</code> request).</li>
        <li><code>304</code>: The resource was not modified. You can use the cached version.</li>
        <li><code>400</code>: Bad request. This could be caused by various actions by the user, such as providing invalid JSON
            data in the request body, providing invalid action parameters, etc.</li>
        <li><code>401</code>: Authentication failed.</li>
        <li><code>403</code>: The authenticated user is not allowed to access the specified API endpoint.</li>
        <li><code>404</code>: The requested resource does not exist.</li>
        <li><code>405</code>: Method not allowed. Please check the <code>Allow</code> header for the allowed HTTP methods.</li>
        <li><code>415</code>: Unsupported media type. The requested content type or version number is invalid.</li>
        <li><code>422</code>: Data validation failed (in response to a <code>POST</code> request, for example). Please check the response body for detailed error messages.</li>
        <li><code>429</code>: Too many requests. The request was rejected due to rate limiting.</li>
        <li><code>500</code>: Internal server error. This could be caused by internal program errors.</li>
    </ul>
</div>
