<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Add new basket';
$this->params['breadcrumbs'][] = ['label' => 'Api', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="col-md-4">       
        <div>
            <?=
            Html::input('', 'nameBasket', '', [
                'class' => 'form-control',
                'placeholder' => 'name'
            ])
            ?>
        </div>
        <br />
        <div>
            <?=
            Html::input('', 'capacityBasket', '', [
                'class' => 'form-control',
                'placeholder' => 'capacity'
            ])
            ?>
        </div>
        <br />        
        <div>
            <b>Description:</b> Add new basket.<br />
            <b>URL:</b> /api/basket/add-basket <br />
            <a class="btn btn-success runAddBasket" data-url="/api/basket/add-basket">Run</a>
        </div>
    </div>
    <div class="col-md-8">
        <div id="result">Result</div>          
    </div>
</div>