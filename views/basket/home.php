
<h2>TEST TASK: FRUIT BASKET API</h2>
<br />

<h4>GENERAL DESCRIPTION</h4>

<p>The purpose of the project is to implement a Fruit Basket API. It should contain "Basket" and "Item"
    entities. The basket has id, name, max capacity and contents properties. Item has the type and weight
    properties. Item type can be 'apple', 'orange' or 'watermelon' (in next version there will be more).
</p>
<br />
<h4>REQUIRED FEATURES</h4>
<ul>
    <li>Add new basket. Name and max capacity is required, contents are prohibited.
    <li>View list of existent baskets
    <li>View basket name and contents
    <li>Rename basket
    <li>Remove basket with all contents
    <li>Add the item to the basket (one or a few). If basket cannot fit all new items (based on their
        weights), need to return an error. It should return an error
    <li>Remove item from basket
    <li>UI (we do not require any specific look and feel, only make it neat and usable)
    <li>Test cases
</ul>
<br />
<h4>NOTES</h4>
<ul>
    <li>API should adhere to the RESTful principles, according to your vision
    <li>It should accept and respond with JSON
    <li>All baskets are shared by all users. No authentication needed
        Storage must be in MySQL InnoDB, ensuring integrity of data. But we know, that in the next
        version we may want to switch to MongoDB/Redis (or another NoSQL storage). Migration of
    <li> PHP code, in this case, should be as simple as possible
    <li>You can use any libraries and frameworks
</ul>