<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Baskets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="basket-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Basket', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'capacity',
            [
                'attribute' => 'Contents',
                'value' => function($data) {
                    return $this->render('itemsBasket', [
                                'basketId' => $data->id,
                                'btnAdd' => TRUE,
                                'dataProvider' => new ActiveDataProvider([
                                    'pagination' => FALSE,
                                    'query' => app\models\BasketItems::find()->where(['basket_id' => $data->id]),
                                ])
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
            ],
        ],
    ]);
    ?>
</div>
