<?php

use yii\helpers\Html;
use yii\grid\GridView;

if ($btnAdd) {
    echo Html::a('<span class="glyphicon glyphicon-plus"></span> Add item', '/basket/add-item?id=' . $basketId, [
        'title' => Yii::t('app', 'Add item'),
    ]);
}

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'summary' => '',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'type',
            'value' => function($data) {
        return $data->type0->name;
    },
            'format' => 'raw',
        ],
        'weight',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'buttons' => [
                'delete' => function($url, $data, $key) use ($countEtap, &$index) {
            $url = '/basket/delete-item?id=' . $data->id;
            if ($index == $countEtap) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => 0,
                ]);
            }
        },
            ],
        ],
    ],
]);
?>