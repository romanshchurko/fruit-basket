<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\Basket */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Baskets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="basket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=
        Html::a('<span class="glyphicon glyphicon-plus"></span> Add item', '/basket/add-item?id=' . $model->id, [
            'title' => Yii::t('app', 'Add item'),
            'class' => 'btn btn-success'
        ]);
        ?>        
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'capacity',
            [
                'attribute' => 'Contents',
                'value' => $this->render('itemsBasket', [
                    'basketId' => $model->id,
                    'btnAdd' => FALSE,
                    'dataProvider' => new ActiveDataProvider([
                        'pagination' => FALSE,
                        'query' => app\models\BasketItems::find()->where(['basket_id' => $model->id]),
                            ])
                ]),
                'format' => 'raw',
            ],
        ],
    ])
    ?>

</div>
