<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\BasketItems */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Add item to Basket';
$this->params['breadcrumbs'][] = ['label' => 'Basket', 'url' => ['view', 'id' => $id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="basket-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList(ArrayHelper::map(\app\models\TypeItem::find()->all(), 'id', 'name'), ['prompt' => 'Choose', 'class' => 'form-control']) ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Add', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>